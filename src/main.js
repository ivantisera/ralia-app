import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import vuetify from './plugins/vuetify'
import 'material-design-icons-iconfont/dist/material-design-icons.css' 
// import firebase from "@firebase/app";

Vue.config.productionTip = false;
//Vue.prototype.BASE_URL = "https://calm-falls-92453.herokuapp.com/";
 Vue.prototype.BASE_URL = "http://127.0.0.1:3000/";

let globalData = new Vue({
  data: { $color: '' }
});
Vue.mixin({
  computed: {
    $color: {
      get: function () { return globalData.$data.$color },
      set: function (newColor) { globalData.$data.$color = newColor; }
    }
  }
})

// const firebaseConfig = {
//   apiKey: "AIzaSyBee3fIoAwQsS2vm_g1PhCtfI-IJUDIGbU",
//   authDomain: "ralia-radios.firebaseapp.com",
//   projectId: "ralia-radios",
//   storageBucket: "ralia-radios.appspot.com",
//   messagingSenderId: "41556171512",
//   appId: "1:41556171512:web:7e61b4e5233588e682390d"
// };


new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
