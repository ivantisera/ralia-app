import firebase from "@firebase/app";

require('firebase/auth');
require('firebase/firestore');


const firebaseConfig = {
  apiKey: "AIzaSyBee3fIoAwQsS2vm_g1PhCtfI-IJUDIGbU",
  authDomain: "ralia-radios.firebaseapp.com",
  projectId: "ralia-radios",
  storageBucket: "ralia-radios.appspot.com",
  messagingSenderId: "41556171512",
  appId: "1:41556171512:web:7e61b4e5233588e682390d"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


const auth = firebase.auth();
const db = firebase.firestore();

export { auth, db }