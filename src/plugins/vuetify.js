import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
          light: {
            verdeclaro: '#73bfb8',
            naranja: '#ed7457',
            amarillo: '#ffdd4b',
            verde: '#2a9c8f',
            violeta: '#6c65aa',
            facebook: '#3b5998',
            youtube: '#c4302b',
            instagram: '#405DE6',
          },
        },
      },
});



